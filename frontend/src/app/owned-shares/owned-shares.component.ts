import { Component, OnInit } from '@angular/core';
import { OwnedShare } from "../../../generated/api/models/owned-share";
import { ApiService } from "../../../generated/api/services/api.service";
import { DOCUMENT } from '@angular/common';
//import { DOCUMENT } from "@angular/platform-browser";

@Component({
    selector: 'app-owned-shares',
    templateUrl: './owned-shares.component.html',
    styleUrls: ['./owned-shares.component.scss']
})
export class OwnedSharesComponent implements OnInit {
    public displayColumns: string[] = ['name', 'buyPrice', 'sellPrice', 'buyDate', 'profit', "tax",'actions'];
    public ownedShares?: OwnedShare[];
    public error: string | undefined;

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        this.updateOwnedShares();
        setInterval(() => this.updateOwnedShares(), 1000)
    }

    calcTax(share:OwnedShare):String{
        let loss:number;
        let output:number;
        if(share.buyPrice == null || share.share?.currentPrice==null)
            return "2000";

        if(share.buyPrice < share.share?.currentPrice){
            let profit: number = share.buyPrice-share.share?.currentPrice;

            if (profit >= share.buyPrice*0.5)
                output = profit*0.59
            else
                output = profit*0.79
        }
        else{output = 0;}
        
        return (Math.round(output * 100) / 100).toFixed(2);
    }
  formatDate(source: string): string {
    let date: Date = new Date(source);
    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0');
    return `${dd}-${mm}-${date.getFullYear()}`;
  }

    updateOwnedShares() {
        this.apiService.investmentShareOwnedGet().toPromise()
            .then((ownedShares: OwnedShare[]) => {
                this.ownedShares = ownedShares;
                this.error = undefined;
            })
            .catch(error => {
                this.ownedShares = undefined;
                this.error = error.message;
            });
    }

    sellShare(ownedShareId: string) {
        if (!confirm("Are you sure you want to sell?"))
            return;
        this.apiService.investmentShareSellPost({'body': ownedShareId}).toPromise()
            .catch(error => window.alert(error.message));
    }
}
