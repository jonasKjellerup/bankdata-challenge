import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Account} from "../../../generated/api/models/account";
import {Transaction} from "../../../generated/api/models/transaction";
import {ApiService} from "../../../generated/api/services/api.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  public account!: Account;
  public displayedColumns = ['description', 'amount', 'date', 'category'];

  dataSource!: MatTableDataSource<Transaction>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.updateTransactions();
  }

  formatDate(source: string): string {
    let date: Date = new Date(source);
    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0');
    return `${dd}-${mm}-${date.getFullYear()}`;
  }

  private updateTransactions() {
    this.route.paramMap.subscribe(params => {
      const accountId = params.get('accountId');
      if (accountId) {
        this.apiService.accountAccountIdGet({accountId}).toPromise().then(
          (account: Account) => {
            this.account = account;
            this.apiService.transactionAccountIdGet({accountId}).toPromise().then(
              (transactions: Transaction[]) => {
                this.dataSource = new MatTableDataSource(transactions);
                this.dataSource.paginator = this.paginator;
              }
            );
          }
        );
      }
    });
  }
}
